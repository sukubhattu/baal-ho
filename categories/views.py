from django.shortcuts import render, get_object_or_404
from .models import Category, Company
from django.views.generic import CreateView, UpdateView, DeleteView, ListView, DetailView
from django.http import HttpResponse
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
# Create your views here.
def home(request):
    return HttpResponse("<H1> Hello welcome to my page </H1>")

class CategoryList(ListView):
    model = Category
    context_object_name = 'companies'
    template_name = 'categories/category_list'
    paginate_by = 2

class CompanyList(ListView):
    model = Company
    context_object_name = 'companies'
    template_name = 'categories/company_list.html'
    
    def get_queryset(self):
        self.category = get_object_or_404(Category, name = self.kwargs['category'])
        return Company.objects.filter(category=self.category)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["category"] = self.category 
        return context

class CompanyDetailView(DetailView):
	model = Company
	template_name = 'categories/company_detail.html'    

class CompanyCreateView(LoginRequiredMixin, CreateView):
    model = Company
    template_name = 'categories/company_create.html'
    fields = ['name', 'description', 'category']
    
    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)


class CompanyUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Company
    template_name = 'categories/company_update.html'
    fields = ['name', 'description']

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)

    def test_func(self):
        company = self.get_object()
        if self.request.user == company.owner:
            return True
        return False


class CompanyDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Company
    template_name = 'categories/company_delete.html'
    success_url = '/categories'
    def test_func(self):
    	company = self.get_object()
    	if self.request.user == company.owner:
    		return True
    	return False
    
def search(request):
    company_list = Company.objects.all()
    company_filter = CompanyFilter(request.GET, queryset=company_list)
    return render(request, 'categories/company_search.html', {'filter': company_filter})