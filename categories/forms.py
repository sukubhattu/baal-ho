from django import forms
from .models import CategoryDetail

class CompanyModelForm(forms.ModelForm):
	class Meta:
		model  = CategoryDetail
		fields = ['name', 'description', 'category']